#include <iostream>
#include <memory>
#include <list>

using namespace std;

class Squad {
public:
	enum Fraction {
		MAGISTER,
		PALADIN,
		GODWOKEN

	};
	Fraction fraction;
	int strength;
	int defence;
	int health;
	int dexterity;
	int dodging;
	enum FightType {
		MELEE,
		RANGED
	};
	FightType fightType;
	Squad(Squad::Fraction f, int strength, int defence, int health, int dexterity, int dodging, Squad::FightType ft) {
		this->fraction = f;
		this->strength = strength;
		this->defence = defence;
		this->health = health;
		this->dexterity = dexterity;
		this->dodging = dodging;
		this->fightType = ft;
	};
	Squad() = delete;

	void print() {
		cout << "-------------------------------------------------------------------" << endl;
		switch (this->fraction)
		{
		case Squad::Fraction::GODWOKEN:
			cout << "Fraction: Godwoken" << endl;
			break;
		case Squad::Fraction::MAGISTER:
			cout << "Fraction: Magister" << endl;
			break;
		case Squad::Fraction::PALADIN:
			cout << "Fraction: Paladin" << endl;
			break;
		}

		cout << "Strength: " << this->strength << endl << "Defence: " << this->defence << endl <<
			"Haelth: " << this->health << endl << "Dexterity: " << this->dexterity << endl << "Dodging: " << this->dodging << endl;
		if (this->fightType == Squad::FightType::MELEE) {
			cout << "Fight Type: Melee" << endl;
		}
		else {
			cout << "Fight Type: Ranged" << endl;
		}
		cout << "-------------------------------------------------------------------" << endl;
	};
};

class SquadBuilder {
protected:
	shared_ptr<Squad> squad;
public:
	SquadBuilder() {};
	virtual ~SquadBuilder() {};
	
	shared_ptr<Squad> getSquad() { return this->squad; };
	virtual void createSquad() = 0;
};

class MagisterMeleeBuilder :public SquadBuilder {
public:
	MagisterMeleeBuilder() {};
	~MagisterMeleeBuilder() override {};


	void createSquad() override {
		squad.reset(new Squad(Squad::Fraction::MAGISTER, 14, 13, 12, 11, 10, Squad::FightType::MELEE));
	};
};

class MagisterRangedBuilder :public SquadBuilder {
public:
	MagisterRangedBuilder() {};
	~MagisterRangedBuilder() override {};


	void createSquad() override {
		squad.reset(new Squad(Squad::Fraction::MAGISTER, 12, 12, 10, 11, 13, Squad::FightType::RANGED));
	};
};

class PaladinRangedBuilder :public SquadBuilder {
public:
	PaladinRangedBuilder() {};
	~PaladinRangedBuilder() override {};


	void createSquad() override {
		squad.reset(new Squad(Squad::Fraction::PALADIN, 10, 13, 10, 11, 13, Squad::FightType::RANGED));
	};
};

class PaladinMeleeBuilder :public SquadBuilder {
public:
	PaladinMeleeBuilder() {};
	~PaladinMeleeBuilder() override {};


	void createSquad() override {
		squad.reset(new Squad(Squad::Fraction::PALADIN, 15, 14, 15, 8, 9, Squad::FightType::MELEE));
	};
};

class GodwokenMeleeBuilder :public SquadBuilder {
public:
	GodwokenMeleeBuilder() {};
	~GodwokenMeleeBuilder() override {};


	void createSquad() override {
		squad.reset(new Squad(Squad::Fraction::GODWOKEN, 14, 14, 14, 12, 13, Squad::FightType::MELEE));
	};
};

class GodwokenRangedBuilder :public SquadBuilder {
public:
	GodwokenRangedBuilder() {};
	~GodwokenRangedBuilder() override {};


	void createSquad() override {
		squad.reset(new Squad(Squad::Fraction::GODWOKEN, 12, 13, 14, 10, 10, Squad::FightType::RANGED));
	};
};

class Chief {
public:
	SquadBuilder * squadBuilder;
	Chief(SquadBuilder* sb) {
		squadBuilder = sb;
		squadBuilder->createSquad();
	};
	void setSquadBuilder(SquadBuilder* sb) {
		squadBuilder = sb;
		squadBuilder->createSquad();
	};
	shared_ptr<Squad> getSquad() { return squadBuilder->getSquad(); };
	void createSquad() {
		squadBuilder->createSquad();
	};
	
};



int main() {
	cout << "hello world" << endl;

	//Builders
	MagisterMeleeBuilder mmb;
	MagisterRangedBuilder mrb;
	PaladinMeleeBuilder pmb;
	PaladinRangedBuilder prb;
	GodwokenMeleeBuilder gmb;
	GodwokenRangedBuilder grb;

	//List
	list<shared_ptr<Squad>> squads;

	//Filling
	Chief c(&mmb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&mrb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&pmb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&prb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&gmb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&grb);
	squads.push_back(c.getSquad());

	//Prtinting
	for (const auto &obj : squads) {
		obj->print();
	}

	return 0;
}
