﻿## Описание задания

Задание №2: "Шаблон Builder".

С использованием шаблона проектирования Builder (строитель) реализуйте заполнение коллекции карточек вашего домашнего задания и вывода содержимого в стандартный поток.

## Функция main

```cpp
    int main() {
	cout << "hello world" << endl;

	//Builders
	MagisterMeleeBuilder mmb;
	MagisterRangedBuilder mrb;
	PaladinMeleeBuilder pmb;
	PaladinRangedBuilder prb;
	GodwokenMeleeBuilder gmb;
	GodwokenRangedBuilder grb;

	//List
	list<shared_ptr<Squad>> squads;

	//Filling
	Chief c(&mmb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&mrb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&pmb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&prb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&gmb);
	squads.push_back(c.getSquad());

	c.setSquadBuilder(&grb);
	squads.push_back(c.getSquad());

	//Prtinting
	for (const auto &obj : squads) {
		obj->print();
	}

	return 0;
}
```

